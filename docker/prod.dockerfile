FROM composer:2 AS build

ENV APP_ENV=prod

WORKDIR /opt/megashop

COPY . .


RUN composer install --no-dev --ignore-platform-req=ext-pdo_mysql && \
    composer dump-env prod

# ---

FROM php:8.2-fpm

RUN docker-php-ext-install opcache pdo_mysql

ENV APP_ENV=prod

WORKDIR /opt/megashop

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini" \
    && groupadd -r web-api \
    && useradd -r -g web-api web-api

COPY --from=build /opt/megashop .

RUN mkdir -p ./var \
    && chown -R web-api:web-api ./var

USER web-api

RUN php bin/console cache:warmup

<?php

namespace App\Tests\App\Service;

use PHPUnit\Framework\TestCase;
use App\Service\PriceCalculator;
use App\Entity\{Product, Coupon};

class PriceCalculatorTest extends TestCase
{
    private PriceCalculator $calculator;
    
    private Product $product;
    
    protected function setUp(): void {
        $this->calculator = new PriceCalculator(
            ['RU' => 0.20]
        );
        
        $this->product = new Product(
            "Kimchi Instant Noodles Cup",
            2.0
        );
        
    }
    
    public function testCalcSimple(): void
    {
        $this->assertEqualsWithDelta(
            2.4,
            $this->calculator->calc('RU', $this->product),
            1e-6
        );
    }
    
    public function testCalcFixedCoupon(): void
    {
        $coupon = new Coupon(type: Coupon::TYPE_FIXED, value: 0.5);
        $this->assertEqualsWithDelta(
            1.8,
            $this->calculator->calc('RU', $this->product, $coupon),
            1e-6
        );
    }
    
    public function testCalcFloatingCoupon(): void
    {
        $coupon = new Coupon(type: Coupon::TYPE_FLOATING, value: 0.5);
        $this->assertEqualsWithDelta(
            1.2,
            $this->calculator->calc('RU', $this->product, $coupon),
            1e-6
        );
    }
}

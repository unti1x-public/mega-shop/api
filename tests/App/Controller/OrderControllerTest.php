<?php

namespace App\Tests\App\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use App\Repository\ProductRepository;

class OrderControllerTest extends WebTestCase
{
    private ?int $productId;

    private KernelBrowser $client;

    protected function setUp(): void
    {
        $this->client = self::createClient();
        
        $container = $this->getContainer();
        /** @var ProductRepository $repo */
        $repo = $container->get(ProductRepository::class);
        assert($repo !== null);
        
        $product = $repo->findOneBy(['title' => 'Iphone']);
        assert($product !== null);
        
        $this->productId = $product->getId();
    }
    
    public function testCreateSimple(): void
    {
        $this->client->request(
            'POST',
            '/order',
            server: [
                'HTTP_CONTENT_TYPE' => 'application/json',
                'HTTP_ACCEPT_CONTENT' => 'application/json'
                
            ],
            content: (string)json_encode([
                'product' => $this->productId,
                'tax_number' => 'DE123456789',
                'payment_type' => 'stripe'
            ])
        );
        
        $this->assertResponseIsSuccessful();
                
        $response = $this->client->getResponse();
        /** @var array{'totalPrice': float} $data */
        $data = json_decode((string)$response->getContent(), true);
        
        $this->assertArrayHasKey('totalPrice', $data);
        $this->assertEqualsWithDelta(119.0, $data['totalPrice'], 1e-2);
    }
    
    public function testCreateFailing(): void
    {
        $this->client->request(
            'POST',
            '/order',
            server: [
                'HTTP_CONTENT_TYPE' => 'application/json',
                'HTTP_ACCEPT_CONTENT' => 'application/json'
                
            ],
            content: (string)json_encode([
                'product' => $this->productId,
                'tax_number' => 'DE123456789',
                'payment_type' => 'paypal'
            ])
        );
        
        $this->assertResponseIsUnprocessable();
        
        $response = $this->client->getResponse();
        /** @var array{'message': string} $data */
        $data = json_decode((string)$response->getContent(), true);

        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(
            'Payment failed: Price is too high',
            $data['message']
        );
    }
    
    public function testCreateCoupon(): void
    {
        $this->client->request(
            'POST',
            '/order',
            server: [
                'HTTP_CONTENT_TYPE' => 'application/json',
                'HTTP_ACCEPT_CONTENT' => 'application/json'
                
            ],
            content: (string)json_encode([
                'product' => $this->productId,
                'tax_number' => 'DE123456789',
                'payment_type' => 'stripe',
                'coupon' => 'fixed'
            ])
        );
        
        $this->assertResponseIsSuccessful();
        
        $response = $this->client->getResponse();
        /** @var array{'totalPrice': float} $data */
        $data = json_decode((string)$response->getContent(), true);
        
        $this->assertArrayHasKey('totalPrice', $data);
        $this->assertEqualsWithDelta(
            99.75 * 1.19,
            $data['totalPrice'],
            1e-2
        );
    }
}

<?php
namespace App\Tests\App\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use App\Repository\ProductRepository;

class ProductControllerTest extends WebTestCase
{
    private ?int $productId;

    private KernelBrowser $client;

    protected function setUp(): void
    {
        $this->client = self::createClient();
        
        $container = $this->getContainer();
        /** @var ProductRepository $repo */
        $repo = $container->get(ProductRepository::class);
        assert($repo !== null);
        
        $product = $repo->findOneBy(['title' => 'Iphone']);
        assert($product !== null);
        
        $this->productId = $product->getId();
    }
    
    public function testShowSimple(): void
    {
        $this->client->request(
            'GET',
            "/product/{$this->productId}",
            ['region' => 'DE']
        );
        $response = $this->client->getResponse();

        $this->assertResponseIsSuccessful();
        
        /** @var array{'price': float, 'title': string} $data */
        $data = json_decode((string)$response->getContent(), true);
        $this->assertEqualsWithDelta(
            119.0, 
            $data['price'],
            1e-2
        );
    }
    
    public function testShowFixed(): void
    {
        $this->client->request(
            'GET',
            "/product/{$this->productId}",
            ['region' => 'DE', 'coupon' => 'fixed']
        );
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        
        /** @var array{'price': float, 'title': string} $data */
        $data = json_decode((string)$response->getContent(), true);
        $this->assertEqualsWithDelta(
            99.75 * 1.19,
            $data['price'],
            1e-2
        );
    }
    
    public function testShowFloat(): void
    {
        $this->client->request(
            'GET',
            "/product/{$this->productId}",
            ['region' => 'DE', 'coupon' => 'float']
        );
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        
        /** @var array{'price': float, 'title': string} $data */
        $data = json_decode((string)$response->getContent(), true);
        $this->assertEqualsWithDelta(
            50 * 1.19, 
            $data['price'],
            1e-2
        );
    }
}

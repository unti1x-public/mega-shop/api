<?php

namespace App\Tests\App\Validator;

use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;
use Symfony\Component\Validator\ConstraintValidatorInterface;
use App\Validator\{TaxNumberConstraintValidator, TaxNumberConstraint};

/**
 * @extends ConstraintValidatorTestCase<TaxNumberConstraintValidator>
 */
class TaxNumberConstraintValidatorTest extends ConstraintValidatorTestCase
{
    protected function createValidator(): ConstraintValidatorInterface {
        return new TaxNumberConstraintValidator();
    }
    
    public function testValidate(): void
    {
        $tests = [
            'DE123456789',
            'IT12345678912',
            'GR123456789',
            'FRZZ123456789'
        ];
        $constraint = new TaxNumberConstraint();
        
        foreach ($tests as $test) {
            $this->validator->validate($test, $constraint);
        }
        
        $this->assertNoViolation();
    }
}

<?php

namespace App\Form;

use App\Entity\Order;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\{ChoiceType, TextType};
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use App\Entity\{Product, Coupon};
use App\Repository\CouponRepository;

class OrderFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('payment_type', ChoiceType::class, [
                'choices' => ['paypal', 'stripe'],
                'required' => true
            ])
            ->add('tax_number', TextType::class, ['required' => true])
            ->add('product', EntityType::class, [
                'class' => Product::class,
                'required' => true,
            ])
            ->add('coupon', EntityType::class, [
                'class' => Coupon::class,
                'choice_value' => 'code'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
        ]);
    }
}

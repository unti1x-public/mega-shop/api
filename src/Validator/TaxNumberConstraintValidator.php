<?php
namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class TaxNumberConstraintValidator extends ConstraintValidator
{
    private const PATTERNS = [
        'DE' => '/^\d{9}$/i',
        'IT' => '/^\d{11}$/i',
        'GR' => '/^\d{9}$/i',
        'FR' => '/^[A-Z]{2}\d{9}$/i'
    ];
    
    /**
     * 
     * @param mixed $value
     * @param TaxNumberConstraint $constraint
     * @return void
     * @throws UnexpectedValueException
     */
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (null === $value || '' === $value) {
            return;
        }
        
        if (!is_string($value)) {
            throw new UnexpectedValueException($value, 'string');
        }
        
        $type = substr($value, 0, 2);
        if (!isset(self::PATTERNS[$type])) {
            $this->context->buildViolation('Unsupported region "{{ region }}"')
                ->setParameter('{{ region }}', $type)
                ->addViolation();
        } else {
            $tin = substr($value, 2);
            if(preg_match(self::PATTERNS[$type], $tin) !== 1) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ string }}', $value)
                    ->addViolation();
            }
        }
    }
}

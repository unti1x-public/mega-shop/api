<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class TaxNumberConstraint extends Constraint {
    public string $message = 'The string "{string}" is not a valid TIN';
}

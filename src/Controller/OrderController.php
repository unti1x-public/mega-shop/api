<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\{JsonResponse, Request};
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\{
    UnprocessableEntityHttpException,
    BadRequestHttpException
};
use Doctrine\ORM\EntityManagerInterface;
use App\Form\OrderFormType;
use App\Entity\{Order, Product};
use App\Repository\CouponRepository;
use App\Service\Payment\{
    PaymentProcessorCollection,
    PaymentProcessorInterface,
    PaymentError
};
use App\Service\PriceCalculator;

class OrderController extends AbstractController
{
    /**
     * 
     * @param Request $request
     * @return array<string, mixed>
     * @throws BadRequestHttpException
     */
    private function getJson(Request $request): array
    {
        /** @var array<string, mixed> $data */
        $data = json_decode($request->getContent(), true);
        
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new BadRequestHttpException();
        }

        return $data;
    }


    #[Route('/order', name: 'app_order', methods: ['POST'])]
    public function create(
        Request $request,
        PaymentProcessorCollection $processors,
        PriceCalculator $calculator,
        CouponRepository $couponRepo,
        EntityManagerInterface $session
    ): JsonResponse
    {
        $order = new Order();
        $form = $this->createForm(OrderFormType::class, $order);
        $formData = $this->getJson($request);

        $form->submit($formData);

        if (!$form->isValid()) {
            throw new UnprocessableEntityHttpException(
                "Invalid form data: {$form->getErrors(true, true)}"
            );
        }
        
        $region = substr((string)$order->getTaxNumber(), 0, 2);
        
        /** @var Product $product */
        $product = $order->getProduct();
        
        $price = $calculator->calc(
            $region,
            $product,
            $order->getCoupon()
        );
        $order->setTotalPrice($price);
        
        try {
            /** @var PaymentProcessorInterface $processor */
            $processor = $processors->getProcessor(
                (string)$order->getPaymentType()
            );
            $processor->process(
                (float)$order->getTotalPrice()
            );
            
        } catch (PaymentError $e) {
            throw new UnprocessableEntityHttpException(
                "Payment failed: {$e->getMessage()}"
            );
        }
        
        $session->persist($order);
        $session->flush();
        
        return $this->json($order, 201);
    }
}

<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\{JsonResponse, Request};
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

use App\Entity\{Product, Coupon};
use App\Repository\CouponRepository;
use App\Service\PriceCalculator;

class ProductController extends AbstractController
{
    #[Route(
        '/product/{id<\d+>}',
        name: 'app_product',
        methods: ['GET']
    )]
    public function show(
        Request $request,
        Product $product,
        PriceCalculator $calculator,
        CouponRepository $couponRepo
    ): JsonResponse
    {
        $region = $request->query->getAlpha('region');
        
        if (!$region) {
            throw new UnprocessableEntityHttpException(
                'Region is required'
            );
        }
        
        $coupon = null;
        $couponCode = $request->query->getAlpha('coupon');
        if ($couponCode) {
            $coupon = $couponRepo->findOneBy(['code' => $couponCode]);
            if (!$coupon) {
                throw new UnprocessableEntityHttpException(
                    'Invalid coupon'
                );
            }
        }
        
        return $this->json([
            'title' => $product->getTitle(),
            'price' => $calculator->calc($region, $product, $coupon),
        ]);
    }
}

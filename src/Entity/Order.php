<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\{Coupon, Product};
use App\Validator\TaxNumberConstraint;

#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ORM\Table(name: '`order`')]
class Order
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: Product::class)]
    #[ORM\JoinColumn(name: "product_id", referencedColumnName: "id")]
    private ?Product $product = null;
    
    #[ORM\ManyToOne(targetEntity: Coupon::class)]
    #[ORM\JoinColumn(name: "coupon_id", referencedColumnName: "id")]
    private ?Coupon $coupon = null;

    #[ORM\Column(length: 16)]
    private ?string $paymentType = null;

    #[ORM\Column]
    private ?int $totalPrice = null;

    #[ORM\Column(length: 32)]
    #[TaxNumberConstraint]
    private ?string $taxNumber = null;

    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getPaymentType(): ?string
    {
        return $this->paymentType;
    }

    public function setPaymentType(string $paymentType): self
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    public function getTotalPrice(): ?float
    {
        return $this->totalPrice / 100;
    }

    public function setTotalPrice(float $totalPrice): self
    {
        $this->totalPrice = (int)($totalPrice * 100);

        return $this;
    }

    public function getTaxNumber(): ?string
    {
        return $this->taxNumber;
    }

    public function setTaxNumber(string $taxNumber): self
    {
        $this->taxNumber = $taxNumber;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getCoupon(): ?Coupon
    {
        return $this->coupon;
    }

    public function setCoupon(?Coupon $coupon): self
    {
        $this->coupon = $coupon;

        return $this;
    }
}

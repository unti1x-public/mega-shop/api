<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use App\Entity\{Product, Coupon};

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $manager->persist(new Product("Iphone", 100.0));
        $manager->persist(new Product("Наушники", 20.0));
        $manager->persist(new Product("Чехол", 10.0));
        
        $manager->persist(new Coupon('fixed', Coupon::TYPE_FIXED, 0.25));
        $manager->persist(new Coupon('float', Coupon::TYPE_FLOATING, 0.5));
        
        $manager->flush();
    }
}

<?php

namespace App\Service\Payment;

use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;


class PaymentProcessorCollection {
    /**
     * 
     * @var PaymentProcessorInterface[]
     */
    private array $processors;
    
    /**
     * 
     * @param \Iterator<PaymentProcessorInterface> $processors
     */
    public function __construct(
        #[TaggedIterator('payment.processor', indexAttribute: 'key')] 
        iterable $processors
    ) {
        $this->processors = iterator_to_array($processors);
    }
    
    public function getProcessor(string $key): ?PaymentProcessorInterface
    {
        return $this->processors[$key];
    }
}

<?php
namespace App\Service\Payment;

interface PaymentProcessorInterface {
    /**
     * 
     * @param float $price
     * @throws PaymentError
     * @return void
     */
    public function process(float $price): void;
}

<?php
namespace App\Service\Payment;

use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;

#[AutoconfigureTag('payment.processor', ['key' => 'stripe'])]
class StripePaymentProcessor implements PaymentProcessorInterface
{
    public function process(float $price): void
    {
        if ($price < 10.0) {
            throw new PaymentError('Price is too low');
        }
        
        // pass
    }
}

<?php
namespace App\Service\Payment;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;

#[AutoconfigureTag('payment.processor', ['key' => 'paypal'])]
class PaypalPaymentProcessor implements PaymentProcessorInterface
{
    public function process(float $price): void
    {
        if ($price > 100.0) {
            throw new PaymentError('Price is too high');
        }
        
        // pass
    }
}

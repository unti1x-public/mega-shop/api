<?php

namespace App\Service;

use App\Entity\{Product, Coupon};

class PriceCalculator {

    /**
     * 
     * @param float[] $rates
     */
    public function __construct(
        private array $rates
    ) {
    }
    
    private function getCouponValue(float $price, Coupon $coupon): float
    {
        $value = match ($coupon->getType()) {
            Coupon::TYPE_FIXED => min($price, (float)$coupon->getValue()),
            Coupon::TYPE_FLOATING => $price * (float)$coupon->getValue(),
            default => 0.0
        };
        
        return $value;
    }

    public function calc(
        string $region,
        Product $product,
        ?Coupon $coupon = null
    ): float
    {
        if (!isset($this->rates[$region])) {
            throw new \ValueError(
                "Unknown region code `$region`"
            );
        }
        
        $price = (float)$product->getPrice();
        
        if ($coupon !== null) {
            $price -= $this->getCouponValue($price, $coupon);
        }
        
        return round($price * ($this->rates[$region] + 1), 2);
    }
}

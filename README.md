# MegaShop

## Постановка задачи

Требуется написать REST API интернет-магазина, который сможет:

1. Возвращать цены отдельных продуктов с учётом НДС
2. Оформлять заказы и проводить оплату
    * при оформлени может быть указан купон на скидку

Вне рамок задачи оставлено следующее:

* Авторизация и аутентификация
* Возврат информации о заказах
* UI админки и самого магазина
* Таймстемпы, soft delete
* Аналитика и метрики
* Обмены данными, такие, как загрузка цен
* Репликация базы, распределение нагрузок и прочие специфичные архитектурные задачи
* Работа со странами, кроме указанных в задаче


### Схема данных

#### Товар

* ID
* Название
* Цена
    * в реальных задачах цены, как правило, хранятся отдельно. оставлено для простоты

#### Купон

* ID
* Код купона
* Тип (фиксированный, плавающий в процентах)

#### Заказ

* ID
* ID товара
* ID купона, если указан
* Способ оплаты (paypal, stripe)
* Итоговая цена с учётом купона и НДС
* Налоговый номер

### Формат налогового номера и размер налога

* В общем виде `([A-Z]{2})([A-Z0-9]+)`, где первые два знака - код региона
* Германия
    * `DE(\d{9})`
    * 19%
* Италия
    * `IT(\d{11})`
    * 22%
* Греция
    * `GR(\d{9})`
    * 24%
* Франция
    * `FR([A-Z]{2}\d{9})`
    * 20%

## Настройка и запуск

Через отладочный docker-compose:

```bash
cp .env .env.local
sed -i "s/^APP_SECRET=$/APP_SECRET=$(openssl rand -hex 32)/" .env.local
docker run docker run --rm --interactive --tty \
  --volume $PWD:/app \
  composer install
docker compose up -d
docker compose exec api bin/console doctrine:migrations:migrate -n
docker compose exec api bin/console doctrine:fixtures:load -n
```

Сервер запустится на http://localhost:8000

Вручную:

```bash
# контейнер базы данных
docker run --name megashop \
    -e MYSQL_ROOT_PASSWORD=megashop \
    -e MYSQL_USER=megashop \
    -e MYSQL_PASSWORD=megashop \
    -e MYSQL_DATABASE=megashop \
    -p 3306:3306 -d mariadb

cp .env .env.local
sed -i "s/^APP_SECRET=$/APP_SECRET=$(openssl rand -hex 32)/" .env.local

composer install
bin/console doctrine:migrations:migrate
bin/console doctrine:fixtures:load -n
php -S localhost:8000 public/index.php
```


## API

### GET /product/{id}

Получение информации о продукте, включая цену с ндс и купоном.

Query-параметры:

* `region` [`str`] - код региона, обязательное
* `coupon` [`str`] - код купона на скидку
    * в фикстуре создаются купоны с кодами `fixed` (0.25 центов) и `float` (50%)

Результат:

* `title` [`str`] - наименование товара
* `price` [`float`] - цена

Пример запроса:

```shell
curl "http://localhost:8000/product/7?region=DE&coupon=float"
```

```json
{"title":"Iphone","price":59.5}
```

### POST /order

Создание заказа и попытка отправки платежа

Структура запроса:

* `payment_type` [`string`] - способ оплаты (`"paypal"` или `"stripe"`)
* `product` [`integer`] - ID товара
* `tax_number` [`string`] - TIN
* `coupon` [`string`] - код купона

Результат: объект заказа

Пример запроса:

```shell
curl -v  -X POST \
     -H "Content-Type: application/json" \
     -d '{"payment_type": "paypal", "product": 8, "tax_number": "DE123456789", "coupon": "fixed"}' \
     http://localhost:8000/order
```

```json
{
    "id":31,
    "paymentType":"paypal",
    "totalPrice":23.5,
    "taxNumber":"DE123456789",
    "product":{
        "id":8,
        "title":"\u041d\u0430\u0443\u0448\u043d\u0438\u043a\u0438",
        "price":20
    },
    "coupon":{"id":1,"code":"fixed","type":"fixed","value":0.25}
}
```
